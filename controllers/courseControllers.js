const mongoose =require("mongoose");
const Course = require("../models/course.js");

//Function for adding a course

// Function for adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course.
module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse,error)=>{
		if(error){
			return error;

		}
		else{
			return newCourse;
		}
	})
}
//Get all Course
module.exports.getAllCourses =()=>{
	return Course.find({}).then(result=>{
		return result;
	})
}

//Get all active courses

module.exports.getActiveCourses =()=>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	})
}


//Get specific course
module.exports.getCourses =(courseId)=>{
	return Course.findById(courseId).then(result=> {
		return result;
	})
}

//updating a course

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//_______________________S40 Activity__________________

module.exports.updatedCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				isActive: newData.course.isActive
				// name: newData.course.name, 
				// description: newData.course.description,
				// price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return false
		})
	}
	else{
		let message = Promise.resolve('you are the admin');
		return message.then((value) => {return value});
	}
}