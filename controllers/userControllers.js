const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
		//
		})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}

		})
	}
//for postman s37 checkemail
		module.exports.checkEmailExist = (reqBody) =>{
			return User.find({email: reqBody.email}).then(result => {
				if(result.length > 0){
					return true;
				}
				else{
					return false;
				}
			})
		}
//for postman s37 login
		module.exports.loginUser = (reqBody)=>{
			return User.findOne({email: reqBody.email}).then(result =>{
				if (result == null){
					return false;
				}
				else{
					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
					const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password );
					//true of false

					if(isPasswordCorrect){
						//lets give the user a token to a access features
						return {access: auth.createAccessToken(result)};
					}
					else{
						//if the password does not match, else
						return false;
					}
				}
			})

		}



//--------------------------
	






// 		module.exports.getAllUserController = (req, res) => {
// 	User.find({})
// 	.then(result => res.send(result))
// 	.catch(error => res.send(error));
// }






// 		module.exports.loginDetails = (reqBody)=>{
// 			return User.findById(reqBody._id).then(result =>{
// 				if (result == null){
// 					return false;
// 				}
// 				else{
// 					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
// 					const isIdCorrect = bcrypt.compareSync(reqBody.Id, result.Id );
// 					//true of false

// 					if(isIdCorrect){
// 						//lets give the user a token to a access features
// 						return {access: auth.createAccessToken(result)};
// 					}
// 					else{
// 						//if the password does not match, else
// 						return false;
// 					}
// 				}
// 			})
// 		}




//38 Activity
module.exports.getProfile =(reqBody)=>{
	return User.findById(reqBody._id).then((result, err)=> {
		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result;
		}
	})
}