const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");

router.post("/register", (request, response)=>{
	userControllers.registerUser(request.body).then(resultFromControllers => response.send(
		resultFromControllers));

})



router.post("/checkEmail", (request, response)=>{
	userControllers.checkEmailExist(request.body).then(resultFromControllers => response.send(resultFromControllers));
})

router.post("/login", (request, response)=>{
	userControllers.loginUser(request.body).then(resultFromControllers => response.send(resultFromControllers));
})


//__________________________________


router.post("/details",(request,response) => {
	userControllers.getProfile(request.body).then(resultFromControllers =>response.send(resultFromControllers))
})





module.exports = router;
