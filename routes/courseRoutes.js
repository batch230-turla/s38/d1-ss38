const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth =require("../auth.js");

// 1. Refractor the "course" route to implement user authentication for the admin when creating a course.

//creating a course

//___________________________


router.post("/newCourse", (request, response)=>{
	courseControllers.addCourse(request.body).then(resultFromController =>response.send(
			resultFromController))
});



//___________________________
router.post("/create", (request, response)=>{
	courseControllers.addCourse(request.body).then(resultFromController =>response.send(
			resultFromController))
});

//Get all courses
router.get("/all",(request, response)=>{
	courseControllers.getlAllCourses().then(resultFromController =>response.send(resultFromController))
})


//Get all ACtivee courses

router.get("/active",(request, response)=>{
	courseControllers.getActiveCourses().then(resultFromController =>response.send(resultFromController))
});

router.get("/:courseId", (request, response)=>{
	courseControllers.getCourses(request.params.courseId).then(resultFromController => response.send(resultFromController))
});
router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


//______________________s40 act_______________________

router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updatedCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;